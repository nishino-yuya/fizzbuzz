package fizzBuzz;

public class FizzBuzz {
	// フィールド
	static String strNumber;
	public static void main(String[] args) {
		System.out.println(fizzBuzz(1));
	}

	// FizzBuzzのメソッド
	public static String fizzBuzz(int number) {
		if((number % 3 == 0) && (number % 5 == 0)) {
			return "FizzBuzz";
		}else if(number % 3 == 0) {
			return "Fizz";
		}else if(number % 5 == 0) {
			return "Buzz";
		}else{
			strNumber = String.valueOf(number);
			return strNumber;
		}
	}

}